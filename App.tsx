/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */
import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Routes from './src/navigation/routes';
import HomeScreen from './src/screens/home';
import QuestionScreen from './src/screens/question';
import ResultScreen from './src/screens/result';

declare const global: {HermesInternal: null | {}};

const Stack = createStackNavigator();

const App = (): JSX.Element => {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none">
        <Stack.Screen name={Routes.HOME_SCREEN} component={HomeScreen} />
        <Stack.Screen
          name={Routes.QUESTION_SCREEN}
          component={QuestionScreen}
        />
        <Stack.Screen name={Routes.RESULT_SCREEN} component={ResultScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
