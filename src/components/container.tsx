import React from 'react';
import {StyleSheet, SafeAreaView, StatusBar, ScrollView} from 'react-native';
import Colors from '../constants/colors';

const Container = (props: any) => {
  const getBackgroundColor = (isHealthy: Boolean | undefined) => {
    if (isHealthy === undefined) {
      return styles.body;
    } else {
      return isHealthy ? styles.bodyHealth : styles.bodyInfected;
    }
  };

  const getStatusBarColor = (isHealthy: Boolean | undefined) => {
    if (isHealthy === undefined) {
      return Colors.defaultColor;
    } else {
      return isHealthy ? Colors.healthyColor : Colors.infectedColor;
    }
  };

  return (
    <SafeAreaView style={getBackgroundColor(props.isHealthy)}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor={getStatusBarColor(props.isHealthy)}
      />
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        {props.children}
      </ScrollView>
    </SafeAreaView>
  );
};

const body = {
  flex: 1,
  backgroundColor: Colors.defaultColor,
};

const styles = StyleSheet.create({
  body: body,
  bodyHealth: {
    ...body,
    backgroundColor: Colors.healthyColor,
  },
  bodyInfected: {
    ...body,
    backgroundColor: Colors.infectedColor,
  },
});

export default Container;
