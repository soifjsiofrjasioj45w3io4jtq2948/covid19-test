import React from 'react';
import {Image, TouchableOpacity, StyleSheet} from 'react-native';

const happy = require('../../assets/happy.png');
const sad = require('../../assets/sad.png');

type buttonType = {
  onPress: (isHappy: Boolean) => void;
  isHappy: Boolean;
  marginStart?: Object;
};

const AnswerButton = (props: buttonType) => {
  return (
    <TouchableOpacity
      style={[styles.button, props.marginStart]}
      onPress={() => props.onPress(props.isHappy)}>
      <Image style={styles.buttonImage} source={props.isHappy ? happy : sad} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    height: 100,
    width: 108,
    borderRadius: 25,
    borderWidth: 3,
    borderStyle: 'solid',
    borderColor: '#ebebeb',
    alignItems: 'center',
    paddingVertical: 10,
  },
  buttonImage: {
    width: 54,
    height: 75,
  },
});

export default AnswerButton;
