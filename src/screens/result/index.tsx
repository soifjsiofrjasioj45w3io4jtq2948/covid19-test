import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import Container from '../../components/container';
import CustomButton from '../../components/customButton';
import Routes from '../../navigation/routes';
import Months from '../../constants/months';
import Colors from '../../constants/colors';

const cross = require('../../../assets/cross.png');
const check = require('../../../assets/check.png');

/* Returns today date as string with specific formatting: Oct 1, 2020 */
const getDate = () => {
  const today = new Date();
  return `${
    Months[today.getMonth()]
  } ${today.getDate()}, ${today.getFullYear()}`;
};

const ResultScreen = ({route, navigation}: any): JSX.Element => {
  const answers = route.params;
  const didPass = answers.indexOf(false) !== -1 ? false : true;

  /* resets screen stack, return home screen */
  const returnHome = () => {
    navigation.reset({
      index: 0,
      routes: [{name: Routes.HOME_SCREEN}],
    });
  };

  return (
    <Container isHealthy={didPass}>
      <View style={didPass ? styles.healthyEllipse : styles.infectedEllipse}>
        <Image
          source={didPass ? check : cross}
          style={didPass ? styles.healthyImg : styles.infectedImg}
        />
      </View>

      <Text style={styles.name}>Jon Marus</Text>
      <Text style={styles.status}>
        {didPass ? 'Health Check Passed' : 'Health Check Failed'}
      </Text>
      <Text style={styles.date}>{getDate()}</Text>

      <CustomButton label={'Done'} onPress={returnHome} />
    </Container>
  );
};

const imgContainer = {
  height: 100,
  width: 100,
  borderRadius: 50,
  marginTop: 200,
};

const styles = StyleSheet.create({
  healthyEllipse: {
    ...imgContainer,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.healthyEllipse,
  },
  infectedEllipse: {
    ...imgContainer,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.infectedEllipse,
  },
  healthyImg: {
    height: 86,
    width: 86,
  },
  infectedImg: {
    height: 56,
    width: 56,
  },
  name: {
    fontSize: 32,
    marginTop: 36,
    alignSelf: 'center',
    fontFamily: 'Inter-Bold',
  },
  status: {
    fontSize: 18,
    alignSelf: 'center',
    fontFamily: 'Inter-Bold',
  },
  date: {
    fontSize: 18,
    marginTop: 20,
    color: 'gray',
    alignSelf: 'center',
    fontFamily: 'Inter-Bold',
    marginBottom: 130,
  },
});

export default ResultScreen;
