import React from 'react';
import {Image, StyleSheet, Text} from 'react-native';
import CustomButton from '../../components/customButton';
import Container from '../../components/container';
import Routes from '../../navigation/routes';

const HomeScreen = ({navigation}: any): JSX.Element => {
  /* Launch questionnaire with question index 0 */
  const beginTest = () => {
    navigation.navigate(Routes.QUESTION_SCREEN, {questionId: 0});
  };

  return (
    <Container>
      <Image
        style={styles.logo}
        source={require('../../../assets/ba-logo-shield-400.png')}
      />
      <Text style={styles.h1}>
        Covid 19{'\n'}
        Health Check
      </Text>

      <Text style={styles.h2}>
        Please answer the following{'\n'}
        questions related to your health{'\n'}
        and Covid 19.
      </Text>

      <CustomButton label={'Begin'} onPress={beginTest} />
    </Container>
  );
};

const styles = StyleSheet.create({
  logo: {
    width: 150,
    height: 150,
    marginTop: 90,
    marginStart: 25,
  },
  h1: {
    fontSize: 36,
    marginTop: 32,
    marginStart: 37,
    fontFamily: 'Inter-Bold',
  },
  h2: {
    fontSize: 18,
    marginStart: 37,
    marginTop: 20,
    marginBottom: 116,
    color: '#343434',
    fontFamily: 'Inter-SemiBold',
  },
});

export default HomeScreen;
