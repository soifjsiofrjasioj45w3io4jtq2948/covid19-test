const Routes = {
  HOME_SCREEN: 'HOME',
  QUESTION_SCREEN: 'QUESTION',
  RESULT_SCREEN: 'RESULT',
};

export default Routes;
