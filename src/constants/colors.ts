const Colors = {
  healthyColor: '#effbf2',
  healthyEllipse: '#aee9bd',
  infectedColor: '#fff3f3',
  infectedEllipse: '#febcbd',
  defaultColor: '#fefefe',
};

export default Colors;
