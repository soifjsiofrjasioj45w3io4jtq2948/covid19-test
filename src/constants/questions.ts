const Questions = [
  {
    text:
      'Have you or anyone in\nyour household had a sore\nthroat, cough, chills, body\naches, shortness of\nbreath, loss of smell, loss\nof taste, fever at or\ngreater than 100º\nFahrenheit (38º Celcius)\nin the last 14 days?',
  },
  {
    text:
      'Have you or anyone in\nyour household tested\npositive for COVID-19 in\nthe las 14 days?',
    bold: ['positive'],
  },
  {
    text:
      'To the best of your\nknowledge have you been\nin close proximity, in the\nlast 14 days, to any\nindividual who tested\npositive for COVID-19?',
  },
];

export default Questions;
